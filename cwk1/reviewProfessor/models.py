from django.db import models
from django.contrib.auth.models import User

class Professor(models.Model):
	professorCode = models.CharField(max_length=120, primary_key=True)
	firstName = models.CharField(max_length=120)
	lastName = models.CharField(max_length=120)

	def __str__(self):
		return self.firstName + " " + self.lastName

class Module(models.Model):
	moduleCode = models.CharField(max_length=120, primary_key=True)
	moduleName = models.CharField(max_length=120)

	def __str__(self):	
		return self.moduleCode + " " + self.moduleName

class ModuleInstance(models.Model):
	moduleCode = models.ForeignKey(Module, on_delete=models.CASCADE)
	professorCode = models.ManyToManyField(Professor)
	year = models.IntegerField()
	semester = models.IntegerField()

	def __str__(self):
		return "<module code: " + str(self.moduleCode) + " | professor: " + str(self.professorCode.all()) + " | " + str(self.semester) + " " + str(self.year) + ">" 

class Review(models.Model):
	ModuleInstanceID = models.ForeignKey(ModuleInstance, on_delete=models.CASCADE)
	userID = models.ForeignKey(User, on_delete=models.CASCADE) #check for user spam
	professorCode = models.ForeignKey(Professor, on_delete=models.CASCADE)
	reviewScore = models.IntegerField()

	def __str__(self): 
		return "PROFESSOR: " + str(self.professorCode) + " | MODULE INSTANCE: " + str(self.ModuleInstanceID) + " | " "user: " + str(self.userID) + " | " + str(self.reviewScore) 
