from django.shortcuts import render
from django.http import *
from django.views.decorators.csrf import csrf_exempt
import json

from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from django.contrib.auth import authenticate, login, logout
from django.contrib.sessions.models import Session
from django.core.exceptions import ObjectDoesNotExist
from reviewProfessor.models import Review, ModuleInstance, Professor

@csrf_exempt
def RateService (request):

	fields = ['professorID', 'moduleCode', 'year', 'semester', 'rating']
	userInput = []

	# possible return data (simple)	
	payloadSuccessfulRating = {'Success': 'Rating has now been added'}
	payloadInvalidType = {'Error': 'Invalid request type'}
	payloadInvalidYear = {'Error': 'Expecting year field to be of Int type'}
	payloadInvalidSemester = {'Error': 'Expecting semester field to be of Int type'}
	payloadInvalidRating = {'Error': 'Expecting rating field to be of whole number and between 1-5'}
	payloadInvalidModule = {'Error': 'Module does not exists, please try again'}
	payloadInvalidReview = {'Error': 'User has already submitted review for this module and lecturer'}

	# checking for correct method type
	if (request.method != 'POST'):
		# response
		http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidType))
		http_bad_response['Content-Type'] = 'application/json'
		return http_bad_response

	else:
		# try and produce a json object from the request body
		# catch exception and output this to user in a readable format
		#
		# check if the object contains each element from fields list and add the input to a list
		# if json object doesnt contain each of these fields return bad request to user
		#
		# check if rating field is valid (integers and rating between 1-5) <- throw error if no valid
		# check to see if there exists a module instance <- throw error if this isnt the case
		# check to see if there exists a record of a rating being made by the user for the module instance <- throw error if so
		# otherwise add to the datanbase


		try: 
			body_data = json.loads(request.body)
		except Exception as e:
			#response
			payloadInvalidJSONFormat = {'Error': '{0}'.format(str(e))}
			http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidJSONFormat))
			http_bad_response['Content-Type'] = 'application/json'	
			return http_bad_response

		for field in fields:
			if(field in body_data):
				userInput.append(body_data.get(field))
			else:
				#response
				payloadInvalidInput = {'Error': 'Expecting ' + field + " field"}
				http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidInput))
				http_bad_response['Content-Type'] = 'application/json'	
				return http_bad_response


		# print( isinstance(userInput[4], int) )
		try:
			int(userInput[4])
		except Exception as e:
			http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidRating))
			http_bad_response['Content-Type'] = 'application/json'	
			return http_bad_response

		validRating = CheckRating( int(userInput[4]) )

		if not validRating:
			http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidRating))
			http_bad_response['Content-Type'] = 'application/json'	
			return http_bad_response

		else:
			moduleInstanceExists = ModuleInstance.objects.filter(professorCode=userInput[0], moduleCode=userInput[1], year=userInput[2], semester=userInput[3]).exists()
			if not moduleInstanceExists:
				http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidModule))
				http_bad_response['Content-Type'] = 'application/json'	
				return http_bad_response

			moduleInstanceGET = ModuleInstance.objects.get(professorCode=userInput[0], moduleCode=userInput[1], year=userInput[2], semester=userInput[3])
			professorCode = Professor.objects.get(professorCode=userInput[0])

			moduleInstance = ModuleInstance.objects.filter(professorCode=userInput[0], moduleCode=userInput[1], year=userInput[2], semester=userInput[3]).values('id')			
			instanceID = moduleInstance[0]['id']

			reviewExists = Review.objects.filter(ModuleInstanceID=instanceID, professorCode=userInput[0], userID=request.user).exists()

			if reviewExists:
				http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidReview))
				http_bad_response['Content-Type'] = 'application/json'	
				return http_bad_response
			else:
				
				newRating = Review.objects.create(ModuleInstanceID=moduleInstanceGET, professorCode=professorCode, userID=request.user, reviewScore=userInput[4])
				newRating.save()

				# response
				http_response = HttpResponse(json.dumps(payloadSuccessfulRating))
				http_response['Content-Type'] = 'application/json'
				http_response.status_code = 200
				http_response.reason_phrase = 'OK'
				return http_response
	
	# fields = ['professorID', 'moduleCode', 'year', 'semester', 'rating']



def CheckRating(rating):

	if CheckInt(rating) and rating <= 5 and rating >= 1:
		return True
	else:
		return False

def CheckInt(value):

	if isinstance(value, int):
		return True
	else:
		return False