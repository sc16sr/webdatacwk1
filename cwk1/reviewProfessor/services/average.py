from django.shortcuts import render
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from decimal import Decimal, ROUND_HALF_UP
import json

from reviewProfessor.models import ModuleInstance, Module, Professor, Review
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from django.contrib.auth import authenticate, login, logout
from django.contrib.sessions.models import Session
from django.core.exceptions import ObjectDoesNotExist

def AverageService (request):

	queryProfessor = request.GET.get('professorCode')
	queryModule = request.GET.get('moduleCode')

	payloadInvalidType = {'Error': 'Invalid request type'}
	payloadNotLoggedIn = {'Error': 'User is not logged in, please login'}
	payloadAllowed = {'Success': 'User is authentic'}
	payloadInvalidInput = {'Error': 'moduleCode or professorCode has not been provided'}
	payloadNoResults = {'Error': 'No reviews found for this professor on this module'}

	# check for the correct method type
	if (request.method != 'GET'):
		# response
		http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidType))
		http_bad_response['Content-Type'] = 'application/json'
		return http_bad_response

	# check to ensure a moduleCode and professorCode has been provided
	elif(queryProfessor is None or queryModule is None):
		# response
		http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidInput))
		http_bad_response['Content-Type'] = 'application/json'
		return http_bad_response

	# check if the user is autheticated
	# if so, get a count of the number of rows in the moduleInstance table based off the module code and professor code provided
	# check to make sure this count is more than 0 <- throw error if not
	# otherwise get a list of all these records 
	# then we get the module name from the module table 
	# then we get the professors name from the professor table

	# we then loop through the list of records
	# we then extract the extract all the records in review table for this module instance and professor.
	# we loop through all those records adding up all the review scores

	# once the first loop as finished we then want to calculate the average (rounding up when average == .5 or more)

	# the json is then constructed and then sent back to the user


	if request.user.is_authenticated:
		numberOfResults = ModuleInstance.objects.filter(moduleCode=queryModule, professorCode=queryProfessor).count()
		if numberOfResults == 0:
			# response
			http_bad_response = HttpResponseBadRequest(json.dumps(payloadNoResults))
			http_bad_response['Content-Type'] = 'application/json'
			return http_bad_response

		else:
			moduleInstanceFilter = ModuleInstance.objects.filter(moduleCode=queryModule, professorCode=queryProfessor).values('id')
			reviewList = []
			averageScores = []
			total = 0

			moduleName = Module.objects.filter(moduleCode=queryModule).values('moduleName')[0]
			professorName = Professor.objects.filter(professorCode=queryProfessor).values('firstName', 'lastName')[0]

			for record in moduleInstanceFilter:
				reviewQuery = Review.objects.filter(ModuleInstanceID=record['id'], professorCode=queryProfessor).values('reviewScore')
				for reviewRecord in reviewQuery:
					averageScores.append(reviewRecord['reviewScore'])
					total += reviewRecord['reviewScore']

				print( averageScores )

			if len(averageScores) > 0:
				averageScore = total / len(averageScores)
				averageScore = int(Decimal(averageScore).quantize(Decimal('1.'), rounding=ROUND_HALF_UP))
			else:
				averageScore = 0

			item = {'moduleCode': queryModule, 'moduleName': moduleName['moduleName'], 'professorCode': queryProfessor, 'professorFirstName': professorName['firstName'],  "professorLastName": professorName['lastName'], 'reviewAverage': averageScore}
			reviewList.append(item)


			responsePayload = {'professorReviewScore': reviewList}

			# response
			http_response = HttpResponse(json.dumps(responsePayload))
			http_response['Content-Type'] = 'application/json'
			http_response.status_code = 200
			http_response.reason_phrase = 'OK'
			return http_response
		
	else:
		# response
		http_bad_response = HttpResponseBadRequest(json.dumps(payloadNotLoggedIn))
		http_bad_response['Content-Type'] = 'application/json'
		return http_bad_response
