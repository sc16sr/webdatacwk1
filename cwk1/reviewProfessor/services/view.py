from django.shortcuts import render
from django.http import *
from decimal import Decimal, ROUND_HALF_UP
import json

from reviewProfessor.models import Review, Professor
from django.db import models

def ViewService (request):
	# possible return data (simple)
	payloadInvalidType = {'Error': 'Invalid request type'}
	payloadNotLoggedIn = {'Error': 'User is not logged in, please login'}
	payloadAllowed = {'Success': 'User is authentic'}

	# check for the correct method type
	if (request.method != 'GET'):
		# response
		http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidType))
		http_bad_response['Content-Type'] = 'application/json'
		return http_bad_response

	# check if the user is autheticated
	# if so get all professors from professors table
	#
	# loop through each record and get the professors average 
	# after getting the average, cfeate the json object for that professor
	# then add to the list
	#
	# once the loop has finished return good response to user

	if request.user.is_authenticated:
		professorsQuery = Professor.objects.all().values('professorCode', 'firstName', 'lastName')
		reviewList = []

		for record in professorsQuery:
			professorAverage = getProfessorAverage(record['professorCode'])	
			item = {'professorCode': record['professorCode'], 'professorFirstName': record['firstName'],  "professorLastName": record['lastName'], 'reviewScore': professorAverage}
			reviewList.append(item)

		responsePayload = {'professorReviewList': reviewList}

		# response
		http_response = HttpResponse(json.dumps(responsePayload))
		http_response['Content-Type'] = 'application/json'
		http_response.status_code = 200
		http_response.reason_phrase = 'OK'
		return http_response
	
	else:
		# response
		http_bad_response = HttpResponseBadRequest(json.dumps(payloadNotLoggedIn))
		http_bad_response['Content-Type'] = 'application/json'
		return http_bad_response

def getProfessorAverage(professorCodeInput):

	professorsReviewScores = Review.objects.filter(professorCode=professorCodeInput).values('reviewScore')
	reviewScoreList = []
	total = 0

	for record in professorsReviewScores:
		reviewScoreList.append(record['reviewScore'])
		total += record['reviewScore']

	if len(reviewScoreList) > 0:
		averageScore = total / len(reviewScoreList)
		averageScore = int(Decimal(averageScore).quantize(Decimal('1.'), rounding=ROUND_HALF_UP))
		return averageScore
	else:
		return 0