from django.shortcuts import render
from django.http import *
import json

from django.contrib.auth import authenticate, login

# Create your views here.
def LoginService (request):
	
	fields = ['username','password']
	userInput = []

	# possible return data (simple)
	payloadInvalidType = {'Error': 'Invalid request type'}
	payloadInvalidInput = {'Error': 'Json body requires both username and password fields'}
	payloadLoginAttemptFail = {'Error': 'Username or Password are incorrect, please try again'}


	# check for the correct method type
	if (request.method != 'POST'):
		# response
		http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidType))
		http_bad_response['Content-Type'] = 'application/json'
		return http_bad_response

	else:
		# try and produce a json object from the request body
		# catch exception and output this to the user in a readable format
		
		# check if the object contains a username and password and add to a list
		# if json object doesnt contain either username or password, return bad request to user
		#
		# check if there exists a user given username and password (authenticate)
		# if one exists, set a login state for them and return a success response (login)
		# otherwise, sent back invalid response  (bad response)

		try: 
			body_data = json.loads(request.body)
		except Exception as e:
			#response
			payloadInvalidJSONFormat = {'Error': '{0}'.format(str(e))}
			http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidJSONFormat))
			http_bad_response['Content-Type'] = 'application/json'	
			return http_bad_response

		for field in fields:
			if( field in body_data):
				userInput.append(body_data.get(field))
			else:
				#response
				payloadInvalidInput = {'Error': 'Expecting ' + field + ' field'}
				http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidInput))
				http_bad_response['Content-Type'] = 'application/json'	
				return http_bad_response

		user = authenticate(request, username=userInput[0], password=userInput[1])
		if user is not None:
			login(request, user)

			# response
			payloadSuccessfulLogin = {'Success': 'Welcome ' + userInput[0]}
			http_response = HttpResponse(json.dumps(payloadSuccessfulLogin))
			http_response['Content-Type'] = 'application/json'
			http_response.status_code = 200
			http_response.reason_phrase = 'OK'
			return http_response
		else:
			# response			
			http_bad_response = HttpResponseBadRequest(json.dumps(payloadLoginAttemptFail))
			http_bad_response['Content-Type'] = 'application/json'
			return http_bad_response
		

#### old method ####
# queryUsername = request.GET.get('username')
# queryPassword = request.GET.get('password')

# check to ensure a username and password has been provided
# elif(queryPassword is None or queryUsername is None):
# 	# response
# 	http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidInput))
# 	http_bad_response['Content-Type'] = 'application/json'
# 	return http_bad_response