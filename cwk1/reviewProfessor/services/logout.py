from django.shortcuts import render
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.contrib.sessions.models import Session
from django.core.exceptions import ObjectDoesNotExist
import json


def LogoutService(request):
	# possible return data (simple)
	payloadInvalidType = {'Error': 'Invalid request type'}
	payloadInvalidSession = {'Error': 'Session is no longer in use'}

	# checks for correct method type
	if (request.method != 'DELETE'):
		# response
		http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidType))
		http_bad_response['Content-Type'] = 'application/json'
		return http_bad_response

	else:
		user = request.user

		logout(request)

		# response
		payloadLogoutUser = {'Success': 'Goodbye ' + str(user)}
		http_response = HttpResponse(json.dumps(payloadLogoutUser))
		http_response['Content-Type'] = 'application/json'
		http_response.status_code = 200
		http_response.reason_phrase = 'OK'
		return http_response


#### old method - getting username #### 
# checks to see if session key is still active / exists
# try:
# 	session_key = request.session.session_key
# 	session = Session.objects.get(session_key=session_key)

# except ObjectDoesNotExist:
# 	# response
# 	http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidSession))
# 	http_bad_response['Content-Type'] = 'application/json'
# 	return http_bad_response


# finds the users id from the session provided in request
# logout the user (remove their session)
# return ok resposne to the user

# uid = session.get_decoded().get('_auth_user_id')
# user = User.objects.get(pk=uid)
