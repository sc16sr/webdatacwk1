from django.shortcuts import render
from django.http import *
from django.contrib.auth.models import User
from django.core.validators import validate_email
from django.core.exceptions import ValidationError

import json

def RegisterService (request):

	fields = ['username','password','email']
	userInput = []

	# possible return data (simple)	
	payloadInvalidType = {'Error': 'Invalid request type'}
	payloadUserExists = {'Error': 'Username already in use, please choose another'}
	payloadEmailExists = {'Error': 'Email already in use, please choose another'}
	payloadInvalidEmail = {'Error': 'Email is not valid, please try again'}
	payloadInvalidPassword = {'Error': 'Password is not sufficient, please be sure the rules are being followed'}
	payloadSuccessfulLogin = {'Success': 'You have successfully registered to rate your professor'}

	# checking for correct method type
	if (request.method != 'POST'):
		# response
		http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidType))
		http_bad_response['Content-Type'] = 'application/json'
		return http_bad_response

	else:
		# try and produce a json object from the request body
		# catch exception and output this to user in a readable format
		#
		# check if the object contains a username, password and email and add the input to a list
		# if json object doesnt contain either username, password or email return bad request to user
		#
		# check username & email doesnt exist in users table <- throw error if there does
		# check password is sufficient (1 uppercase, 1 lowercase, 1 number, doesnt contain username, longer than 6) <- throw error if not
		# check email is valid
		# add username,password and email to Users table
		# send response

		try: 
			body_data = json.loads(request.body)
		except Exception as e:
			#response
			payloadInvalidJSONFormat = {'Error': '{0}'.format(str(e))}
			http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidJSONFormat))
			http_bad_response['Content-Type'] = 'application/json'	
			return http_bad_response

		for field in fields:
			if( field in body_data):
				userInput.append(body_data.get(field))
			else:
				#response
				payloadInvalidInput = {'Error': 'Expecting ' + field + " field"}
				http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidInput))
				http_bad_response['Content-Type'] = 'application/json'	
				return http_bad_response

		userExists = User.objects.filter(username=userInput[0]).exists()

		emailExists = User.objects.filter(email=userInput[2]).exists()
		
		sufficientPassword = CheckPasswordValidity(userInput[0], userInput[1])

		try:
			validate_email(userInput[2])
		except ValidationError as e:
			http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidEmail))
			http_bad_response['Content-Type'] = 'application/json'
			return http_bad_response


		if userExists:
			http_bad_response = HttpResponseBadRequest(json.dumps(payloadUserExists))
			http_bad_response['Content-Type'] = 'application/json'
			return http_bad_response

		elif emailExists:
			http_bad_response = HttpResponseBadRequest(json.dumps(payloadEmailExists))
			http_bad_response['Content-Type'] = 'application/json'
			return http_bad_response

		elif sufficientPassword:

			newUser = User.objects.create_user(userInput[0], userInput[2], userInput[1])
			newUser.save()

			# response
			http_response = HttpResponse(json.dumps(payloadSuccessfulLogin))
			http_response['Content-Type'] = 'application/json'
			http_response.status_code = 200
			http_response.reason_phrase = 'OK'
			return http_response

		else:
			http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidPassword))
			http_bad_response['Content-Type'] = 'application/json'
			return http_bad_response




def CheckPasswordValidity(username, password):

	lowercaseUsername = username.lower()
	lowercasePassword = password.lower()

	if lowercaseUsername in lowercasePassword:
		return False
	elif len(password) < 8:
		return False
	elif (any(x.isupper() for x in password) and any(x.islower() for x in password) and any(x.isdigit() for x in password) ):
		return True
	else:
		return False


### old method ###
# queryEmail = request.GET.get('email')
# queryUsername = request.GET.get('username')
# queryPassword = request.GET.get('password')

# check to ensure a username, password and email has been provided
# if(queryEmail is None or queryPassword is None or queryUsername is None):
# 	# repsonse
# 	http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidInput))
# 	http_bad_response['Content-Type'] = 'application/json'
# 	return http_bad_response