from django.shortcuts import render
from django.http import *
from django.views.decorators.csrf import csrf_exempt
import json

from reviewProfessor.models import ModuleInstance, Module, Professor
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from django.contrib.auth import authenticate, login, logout
from django.contrib.sessions.models import Session
from django.core.exceptions import ObjectDoesNotExist

def ListService (request):

	# possible return data (simple)
	payloadInvalidType = {'Error': 'Invalid request type'}
	payloadNotLoggedIn = {'Error': 'User is not logged in, please login'}
	payloadAllowed = {'Success': 'User is authentic'}

	# check for the correct method type
	if (request.method != 'GET'):
		# response
		http_bad_response = HttpResponseBadRequest(json.dumps(payloadInvalidType))
		http_bad_response['Content-Type'] = 'application/json'
		return http_bad_response


	# check if the user is autheticated
	# if so, get all rows from the moduleInstancen table
	# loop through each module
	# check if the record does not exist in the module instance list (output list), if so get the list of professors for that module, and then create a JSON object for that module
	# then add the JSON object to the module instance list
	# return response to the user

	print ( "user = " + str(request.user) )
	if request.user.is_authenticated:

		moduleInstanceQuery = ModuleInstance.objects.all().values('year', 'semester' , 'moduleCode', 'professorCode')
		moduleInstanceList = []

		for record in moduleInstanceQuery:
			if not recordExistsInModuleList(record, moduleInstanceList):
				professorList = getProfessorList(record)	
				moduleName = Module.objects.filter(moduleCode=record['moduleCode']).values('moduleName')[0]
				item = {'moduleCode': record['moduleCode'], 'moduleName': moduleName['moduleName'], 'year': record['year'], 'semester': record['semester'], 'professors': professorList}
				moduleInstanceList.append(item)

		responsePayload = {'moduleInstanceList': moduleInstanceList}

		# response
		http_response = HttpResponse(json.dumps(responsePayload))
		http_response['Content-Type'] = 'application/json'
		http_response.status_code = 200
		http_response.reason_phrase = 'OK'
		return http_response
	
	else:
		# response
		http_bad_response = HttpResponseBadRequest(json.dumps(payloadNotLoggedIn))
		http_bad_response['Content-Type'] = 'application/json'
		return http_bad_response



# get the module instance from the ModuleInstance table given the information provided
# loop through the module instance list and get the professors
# we then create a JSON object for that single professor 
# then add to the returned list  
def getProfessorList(record):

	moduleInstanceQuery = ModuleInstance.objects.filter(year=record['year'], semester=record['semester'], moduleCode=record['moduleCode']).values('professorCode')
	professorsList = []

	for newRecord in moduleInstanceQuery:
		professorsInModule = Professor.objects.filter(professorCode=newRecord['professorCode']).values('firstName', 'lastName')[0]
		item = {'professorCode': newRecord['professorCode'], 'professorFirstName': professorsInModule['firstName'],  "professorLastName": professorsInModule['lastName'],}
		professorsList.append(item)
	return professorsList

# we loop for the current module instance list and check if the record we are using at that moment in time is already within 
def recordExistsInModuleList(record, moduleInstanceList):
	recordYear = record['year']
	recordSemester =  record['semester']
	recordModuleCode = record['moduleCode']

	for item in moduleInstanceList:
		if item['year'] == recordYear and item['semester'] == recordSemester and item['moduleCode'] == recordModuleCode:
			return True
	
	return False