from django.views.decorators.csrf import csrf_exempt
import json

from reviewProfessor.services.login import LoginService
from reviewProfessor.services.register import RegisterService
from reviewProfessor.services.logout import LogoutService
from reviewProfessor.services.list import ListService
from reviewProfessor.services.view import ViewService
from reviewProfessor.services.average import AverageService
from reviewProfessor.services.rate import RateService

@csrf_exempt
def Login(request):
	return LoginService(request)

@csrf_exempt
def Register(request):
	return RegisterService(request)

@csrf_exempt
def Logout (request):
	return LogoutService(request)

def List (request):
	return ListService(request)

def View (request):
	return ViewService(request)

def Average (request):
	return AverageService(request)

@csrf_exempt
def Rate (request):
	return RateService(request)


			# theList = []
			# item = {'username': queryUsername, 'password': queryPassword, 'email': queryEmail}
			# theList.append(item)

			# payloadUserRegisteredDetails = {'registerDeatils' : theList}
